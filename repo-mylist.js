import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import 'repo-detectformat/repo-detectformat.js';
import 'gly-validated-message/gly-gtc-vm-hashkey.js';

class RepoMyList extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/repo_registered_text_list_param_email?email={{email}}&graph=http%3A%2F%2Fsparqlite.com%2Frepo&limit=10&offset=0" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <table border="1">
      <thead>
        <tr>
          <th scope="cols">hash</th>
          <th scope="cols">text</th>
          <th scope="cols">format</th>
          <th scope="cols">massage(s)</th>
<!--          <th scope="cols">date</th>  -->
        </tr>
      </thead>
      <tbody>
        <template is="dom-repeat" items="{{sampleids}}">
          <tr>
            <th scope="row">{{item.hashed_text}}</th>
            <th scope="row">{{item.text}}</th>
            <th scope="row"><repo-detectformat hashed_text="{{item.hashed_text}}"></repo-detectformat></th>
            <th scope="row"><gly-gtc-vm-hashkey hashed_text="{{item.hashed_text}}"></gly-gtc-vm-hashkey></th>
<!--            <th scope="row">{{item.date}}</th> -->
          </tr>
        </template>
      </tbody>
    </table>
  </div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      email: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('repo-mylist', RepoMyList);
